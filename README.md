## react-native-tabs

Component for rendering tabs.

## Installation

`npm i git+https://git@gitlab.com/goodproductssoft/react-native-tabs --save`

## Usage 1
```javascript
import { Tabs, Tab } from 'react-native-tabs'
```
```javascript
<Tabs>
    <Tab />
    <Tab />
    <Tab />
</Tabs>
```

## Usage 2

```javascript
import { Tabs, Tab } from 'react-native-tabs'
```
```javascript
const { isShowSelling } = this.state
```
```javascript
<Tabs>
    <Tab
        isActive={!isShowSelling}
        title={'Buying'}
        onPress={() => this.setState({isShowSelling: false})}
    />
    <Tab
        isActive={isShowSelling}
        title={'Selling'}
        onPress={() => this.setState({isShowSelling: true})}
    />
</Tabs>
```