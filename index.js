/**
 * Created by Bardiaswift on 20/09/16.
 */
import React, { PropTypes } from 'react'
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native'

export function Tab(props) {
    const {
      title,
      onPress,
      isActive,
      tabStyle,
      inactiveTabStyle,
      activeTabTextStyle,
      inactiveTabTextStyle,
      titleStyle,
      isShowBottomBorder,
      activeBottomBorderStyleStyle,
      inactiveBottomBorderStyleStyle,
      numberOfLines,
      canPressActive
    } = props
    let style
    let textStyle
    let activeOpacity
    let handler
    let bottomBorderStyle
    if (isActive) {
        textStyle = [Styles.activeTabText, activeTabTextStyle]
        if (canPressActive) {
            handler = onPress
        } else {
            activeOpacity = 1
        }
        bottomBorderStyle = activeBottomBorderStyleStyle
    } else {
        style = [Styles.inactiveTab, inactiveTabStyle]
        textStyle = [Styles.inactiveTabText, inactiveTabTextStyle]
        handler = onPress
        bottomBorderStyle = inactiveBottomBorderStyleStyle
    }
    return (
      <TouchableOpacity style={[Styles.tab, tabStyle, style]} onPress={handler} activeOpacity={activeOpacity}>
          <Text style={[Styles.tabText, textStyle, titleStyle]} numberOfLines={numberOfLines}>{title}</Text>
          {isShowBottomBorder && <View style={[Styles.bottomBorder, bottomBorderStyle]} />}
      </TouchableOpacity>
    )
}

export function Tabs(props) {
    const { style, children } = props
    return (
      <View style={[Styles.container, style]}>
          {children}
      </View>
    )
}

Tab.propTypes = {
    title: PropTypes.string,
    isActive: PropTypes.bool,
    onPress: PropTypes.func,
    tabStyle: PropTypes.any,
    inactiveTabStyle: PropTypes.any,
    activeTabTextStyle: PropTypes.any,
    inactiveTabTextStyle: PropTypes.any,
    isShowBottomBorder: PropTypes.bool,
    activeBottomBorderStyleStyle: PropTypes.any,
    inactiveBottomBorderStyleStyle: PropTypes.any,
    isShowBottomBorder: PropTypes.bool,
    numberOfLines: PropTypes.number,
    canPressActive: PropTypes.bool
}

Tabs.propTypes = {
    style: PropTypes.any
}

const borderRadius = 3

const Styles = StyleSheet.create({
    container: {
        height: 51,
        backgroundColor: '#FFD90C',
        flexDirection: 'row',
        alignItems: 'center'
    },
    tab: {
        flex: 1,
        height: 51,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    inactiveTab: {
        backgroundColor: '#555555'
    },
    tabText: {
        textAlign: 'center',
        backgroundColor: 'transparent',
        fontSize: 16,
        fontWeight: '600'
    },
    activeTabText: {
        color: '#000',
    },
    inactiveTabText: {
        color: '#FFD90C',
    },
    bottomBorder: {
        position: 'absolute',
        bottom: -1,
        left: 0,
        right: 0,
        height: 1,
        backgroundColor: 'lightgray'
    }
})
